#!/usr/bin/env python
################################################################
# BGCDiff: A pipeline to quantify biosynthetic gene clusters in whole-metagenome shotgun sequencing samples
# Copyright (C) <2017> Mohamad Koohi-Moghadam and Junwen Wang Lab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################
import distutils.spawn
import os
import shutil
import sys
import time
import argparse
from src import functions

VERSION = "0.1.3"

################################################################
# Set the arguments
parser = argparse.ArgumentParser(description='BGCQuantify: This script generates the abundance scores for BGCs in a '
                                             'whole metagenome shotgun (WMS) sample. '
                                             'The input WMS sample can be in Fasta or Fastq format and BGCs of '
                                             'interest can be in Fasta or Bowtie2 index file. '
                                             'As output the result in CSV format')
parser.add_argument("--version", action="version", version="%(prog)s v" + VERSION)

# The input args
inputArgs = parser.add_argument_group('Input:')
inputArgs.add_argument('--bgc', type=str, dest='strBGC',
                       help='Enter the path and name of the BGCs of interest in Fasta format. (i.e. '
                            '~/home/data/bgc_file.fasta)')
inputArgs.add_argument('--bgcIndex', type=str, dest='strBGCIndex',
                       help='Enter the path of the BGCs of interest in Bowtie2 index format. (i.e. '
                            '~/home/data/bgcIndexDB)')
inputArgs.add_argument('--sampleDir', type=str, dest='strSampleDir',
                       help='Enter the path of the folder that contains sample folders. (i.e. ~/home/data/case)')
inputArgs.add_argument('--sample', type=str, dest='strSample',
                       help='Enter the path of the folder that contain nucleotide reads. The read files should be in '
                            'Fasta or Fastaq format. (i.e. ~/home/data/sample1)')

# The output args
outputArgs = parser.add_argument_group('Output:')
outputArgs.add_argument('--tmp', type=str, dest='strTmp',
                        help='Enter the path of the temp directory. (i.e. ~/home/output/temp)')

outputArgs.add_argument('--output', type=str, dest='strOut',
                        help='Enter the path of the output directory. (i.e. ~/home/output/result)')
# The tools args
toolsArgs = parser.add_argument_group('Programs:')
toolsArgs.add_argument('--bowtie2', default=str((distutils.spawn.find_executable("bowtie2"))).replace("/bowtie2", ""),
                       type=str, dest='strBowtie2',
                       help='Provide the path to the bowtie2 folder (i.e. ~/home/tools/Bowtie2). Default call will be '
                            '\"bowtie2-build\" to make the bowtie2 index files and \"bowtie2\" to run the tool.')
toolsArgs.add_argument('--bedtools',
                       default=str((distutils.spawn.find_executable("bedtools"))).replace("/bedtools", ""), type=str,
                       dest='strBedtools',
                       help='Provide the path to BEDtools folder (i.e. ~/home/tools/Bedtools). Default call will be '
                            '\"bedtools\".')
toolsArgs.add_argument('--samtools',
                       default=str((distutils.spawn.find_executable("samtools"))).replace("/samtools", ""), type=str,
                       dest='strSamtools',
                       help='Provide the path to SAMtools (i.e. ~/home/tools/Samtools). Default call will be \"samtools\".')
# The parameters args
paramArgs = parser.add_argument_group('Parameters:')
paramArgs.add_argument('--WMSType', type=str, dest='strWMSType', help='Enter the type of WMS. \"f\" for the Fasta WMS '
                                                                      'and \"q\" for the Fastq one. The default '
                                                                      'setting is Fasta file', default="f")
paramArgs.add_argument('--identity', type=float, dest='strIdentity', help='Enter the minimum percent of identity for '
                                                                          'the matched hits. The default is 0.95.',
                       default=0.95)
paramArgs.add_argument('--alignlen', type=float, dest='strAlnLength', help='Enter the minimum alignment length for '
                                                                           'the matched hits. The default is 60.',
                       default=60)
paramArgs.add_argument('--threads', type=int, dest='strThreads', help='Enter the number of CPUs available for '
                                                                      'parallel processing. The default is 1',
                       default=1)

paramArgs.add_argument('--removeDup', action='store_true', dest='strRemoveDuplicate',
                       help='Using this option, the pipeline will remove potential PCR duplicates from SAM file.')
paramArgs.add_argument('--removeTemp', action='store_true', dest='strRemoveTemp',
                       help='Using this option, the pipeline will remove the temp folders after finishing the process.')

# Check for args.
if len(sys.argv) == 1:
    parser.print_help()
    sys.stderr.write(
        "\nNo arguments were supplied to BGCQuantify.py. Please see the help information above to determine what to "
        "pass to the program.\n")
    sys.exit(1)

############################################################################
args = parser.parse_args()

bgcArgs = len([x for x in (args.strBGC, args.strBGCIndex) if x is not None])
if bgcArgs == 2:
    parser.error('Just one of the --bgc and --bgcIndex must be given.')
if bgcArgs == 0:
    parser.error('At least one of the --bgc or --bgcIndex must be given.')

sampleArgs = len([x for x in (args.strSample, args.strSampleDir) if x is not None])
if sampleArgs == 2:
    parser.error('Just one of the --sample and --sampleDir must be given.')
if sampleArgs == 0:
    parser.error('At least one of the --sample and --sampleDir must be given.')

if args.strOut is None:
    parser.error('--output must be given.')

if args.strWMSType not in 'fq':
    parser.error('--WMSType must be set to the \"f\" or \"q\". It seems you set other values to this option.')

# Check Dependencies
sys.stderr.write("Checking dependencies...\n")
functions.checkForDependency(args.strBowtie2, "-h", "bowtie2")
functions.checkForDependency(args.strBedtools, "-h", "bedtools")
functions.checkForDependency(args.strSamtools, "--help", "samtools")

# Make the output directory
dirOut = args.strOut
functions.createFolder(dirOut)

# Make the temp directory
dirTmp = args.strTmp
if dirTmp is None:
    dirTmp = (dirOut + os.sep + "temp_" + str(os.getpid()) + '%.0f' % round((time.time() * 100), 1))

functions.createFolder(dirTmp)

def runPipeline(dirOut, dirTmp, strSample, samplePrefix, sampleType, bgcDB, strBowtie2, strSAMTools, strBEDTools, strIdentity,
                strAlnLength, strThreads):
    sampleName = str(strSample).split(os.sep)[-1]
    destFolder = dirTmp + os.sep + samplePrefix + "_" + sampleName
    # Run Bowtie2
    sys.stderr.write("Run Bowtie2 on: " + sampleName + "\n")
    bowtieResult, readNumber = functions.runBowtie2(strBowtie2, strSample, sampleType, bgcDB, destFolder,
                                                    strThreads)

    # Sort SAM file
    sys.stderr.write("Sort SAM file for: " + sampleName + "\n")
    sortedSAMFile = functions.sortSAMFile(strSAMTools, bowtieResult, destFolder)

    # Remove duplicate PCR reads. (It is an optional step.)
    if args.strRemoveDuplicate:
        sortedSAMFile = functions.removePCRDuplicate(strSAMTools, sortedSAMFile, destFolder)

    # Process CIGAR
    sys.stderr.write("Process the CIGAR..." + sampleName + "\n")
    CIGAR_Out_SAM, hitDic = functions.CIGARParser(sortedSAMFile, destFolder, strIdentity, strAlnLength)

    # Convert SAM to BAM file
    sys.stderr.write("Convert SAM to BAM..." + sampleName + "\n")
    CIGAR_Out_BAM = functions.convertSAM2BAM(strSAMTools, CIGAR_Out_SAM, destFolder)

    # Run BEDTool
    sys.stderr.write("Run BEDTool on..." + sampleName + "\n")
    BEDtool_out = functions.runBEDTools(strBEDTools, CIGAR_Out_BAM, destFolder)

    # Calculate abundance score
    sys.stderr.write("Calculate abundance score..." + sampleName + "\n")
    resultDF = functions.abundanceScore(BEDtool_out, hitDic, readNumber)

    resultDF.to_csv(dirOut + os.sep + sampleName + ".csv")

    if args.strRemoveTemp:
        shutil.rmtree(dirTmp)

start_time = time.time()
bgcDB = ""

# Make the Bowtie2 DB for BGCs of interest
sys.stderr.write("Build the Bowtie2 DB. Please wait...\n")
if args.strBGC is not None:
    bgcDB = functions.createBowtie2IndexDB(args.strBowtie2, args.strBGC, dirTmp + os.sep + "IndexDB", "BGCDB")
else:
    bgcDB = args.strBGCIndex

if args.strSample is not None:
    runPipeline(dirOut, dirTmp, args.strSample, "", args.strWMSType, bgcDB, args.strBowtie2, args.strSamtools, args.strBedtools,
                args.strIdentity, args.strAlnLength, args.strThreads)

elif args.strSampleDir is not None:
    sample_dirs = [d for d in os.listdir(args.strSampleDir) if os.path.isdir(os.path.join(args.strSampleDir, d))]
    for currentSample in sample_dirs:
        currentDirTemp = dirTmp + os.sep + currentSample
        runPipeline(dirOut, currentDirTemp, args.strSampleDir + os.sep + currentSample, "", args.strWMSType, bgcDB, args.strBowtie2, args.strSamtools,
                    args.strBedtools,
                    args.strIdentity, args.strAlnLength, args.strThreads)

timeMin = (time.time() - start_time) / 60
sys.stderr.write("Total Time " + str(timeMin) + "\n")
