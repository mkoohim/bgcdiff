#!/usr/bin/env python
################################################################
# BGCDiff: A pipeline to quantify biosynthetic gene clusters in whole-metagenome shotgun sequencing samples
# Copyright (C) <2017> Mohamad Koohi-Moghadam and Junwen Wang Lab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################
import distutils.spawn
import glob
import os
import sys
import argparse
import pandas
import subprocess
from src import functions

VERSION = "0.1.3"
################################################################################
# Set the arguments
parser = argparse.ArgumentParser(description='BGCCompare: This script apply Wilcoxon-rank sum test on the two groups '
                                             'of abundance score two find the BGCs which are statistically different '
                                             'between these two groups. To run the code it needs the result of '
                                             'BGCQuantify outputs for case and control groups.')
parser.add_argument("--version", action="version", version="%(prog)s v" + VERSION)

# The input args
grpInput = parser.add_argument_group('Input:')
grpInput.add_argument('--case', type=str, dest='strCase',
                      help='Enter the path of the BGCQuantify results for the case folder. (i.e. ~/home/data/caseResult)')
grpInput.add_argument('--control', type=str, dest='strControl',
                      help='Enter the path of the BGCQuantify results for the control folder. (i.e. ~/home/data/controlResult)')

# The output args
grpOutput = parser.add_argument_group('Output:')
grpOutput.add_argument('--output', type=str, dest='strOut',
                       help='Enter the path of the output directory.')

# The tools args
grpPrograms = parser.add_argument_group('Programs:')
grpPrograms.add_argument('--rpath', default=str((distutils.spawn.find_executable("Rscript"))).replace("/Rscript",""), type=str, dest='strRscript',
                         help='Provide the path to Rscript. Default call will be \"Rscript\\".')
# The parameters args
grpParam = parser.add_argument_group('Parameters:')
grpParam.add_argument('--t', type=float, dest='strThreshold', help='Enter the threshold to exclude BGCs with average '
                                                                   'abundance score with less than this value.', default=100)

# Check for args.
if len(sys.argv) == 1:
    parser.print_help()
    sys.stderr.write(
        "\nNo arguments were supplied to BGCCompare.py. Please see the help information above to determine what to "
        "pass to the program.\n")
    sys.exit(1)

############################################################################
args = parser.parse_args()

if len([x for x in (args.strCase, args.strControl) if x is not None]) != 2:
    parser.error('Both of the --case and --control must be given.')

# Check Dependencies
sys.stderr.write("Checking dependencies...\n")
functions.checkForDependency(args.strRscript, "--help", "Rscript")

outputFolder = args.strOut
outputFolder = functions.createFolder(outputFolder)

allDataDF = pandas.DataFrame()
allDataDF = functions.analyzeResult(args.strCase, "Case", allDataDF)
allDataDF = functions.analyzeResult(args.strControl, "Control", allDataDF)

allDataDF = allDataDF.set_index('Cluster').transpose()
as_list = allDataDF.index.tolist()

new_index = []
for item in as_list:
    if "Case" in item:
        new_index.append("Case")
    else:
        new_index.append("Control")

allDataDF['Group'] = new_index
selectedBGC = []
for col in allDataDF.columns:
    if 'Group' not in col:
        case = allDataDF.loc[allDataDF['Group'] == "Case", col].values
        control = allDataDF.loc[allDataDF['Group'] == "Control", col].values        
        meanCase = sum(case) / float(len(case))
        meanControl = sum(control) / float(len(control))        
        valResult = col + "\t" + str(meanCase) + "\t" + str(meanControl)
        if meanCase > args.strThreshold or meanControl > args.strThreshold:
            selectedBGC.append(col)

allDataDF.index.name = "Taxa"
allDataDF.to_csv(outputFolder + os.sep + "Score_all.csv")

for column in allDataDF.columns:
    if column not in selectedBGC:
        allDataDF.drop(column, axis=1, inplace=True)
allDataDF.to_csv(outputFolder + os.sep + "Score_selected.csv")

# Run Wilcoxon-Rank sum test on the data
sys.stderr.write("\nWilcoxon-rank sum test (alternative = greater):\n")
functions.runRScript(args.strRscript, outputFolder + os.sep + "Score_selected.csv", outputFolder, "greater")

sys.stderr.write("\nWilcoxon-rank sum test (alternative = less):\n")
functions.runRScript(args.strRscript, outputFolder + os.sep + "Score_selected.csv", outputFolder, "less")

sys.stderr.write("\nWilcoxon-rank sum test (alternative = two.sided):\n")
functions.runRScript(args.strRscript, outputFolder + os.sep + "Score_selected.csv", outputFolder, "two.sided")