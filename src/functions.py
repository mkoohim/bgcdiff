import glob
import os
import sys
import subprocess
import shutil
import pandas
import config
import re


def createBowtie2IndexDB(strBowtie2, strBGC, destFolder, DBName):
    createFolder(destFolder)
    FNULL = open(os.devnull, 'w')
    subprocess.call([strBowtie2 + os.sep + 'bowtie2-build', '-f', strBGC, destFolder + os.sep + DBName],
                    stdout=FNULL, stderr=subprocess.STDOUT)
    return destFolder + os.sep + DBName


def runBowtie2(strBowtie2, strSample, strWMSType, strBGCDB, strOutput, strThread):
    currentSample = str(strSample).split(os.sep)[-1]
    # A variable which saves the number of reads
    readNumber = 0

    # Generate temp folders for the intermediate files
    createFolder(strOutput)
    if strWMSType is 'f':
        resultFiles = glob.glob(strSample + os.sep + "*.fasta")
    else:
        resultFiles = glob.glob(strSample + os.sep + "*.fastq")
    if strWMSType is 'f' and len(resultFiles) == 0:
        raise Exception("The sample folder does not contain any FASTA read files: " + strSample)
    if strWMSType is 'q' and len(resultFiles) == 0:
        raise Exception("The sample folder does not contain any FASTQ read files: " + strSample)
    strFiles = ""
    for file in resultFiles:
        strFiles += file + ","
    strFiles = strFiles.rstrip(",")
    bowtieOut = open(strOutput + os.sep + "temp.txt", "w")
    if strWMSType is 'f':
        subprocess.call([strBowtie2 + os.sep + "bowtie2", "--no-unal", "-f", "-x", strBGCDB, "-U", strFiles, "-S",
                         strOutput + os.sep + "out_bowtie.sam", "-p", str(strThread)], stderr=bowtieOut)
    else:
        subprocess.call([strBowtie2 + os.sep + "bowtie2", "--no-unal", "-x", strBGCDB, "-U", strFiles, "-S",
                         strOutput + os.sep + "out_bowtie.sam", "-p", str(strThread)], stderr=bowtieOut)
    bowtieOut.close()
    bowtieOut = open(strOutput + os.sep + "temp.txt", "r")
    findIt = False
    for line in bowtieOut:
        sys.stderr.write(line + "\n")
        if "reads; of these:" in line:
            findIt = True
            readNumber = int(line.split(" ")[0])
    if not findIt:
        raise Exception("Bowtie2 could not generate result for sample:" + currentSample)
    bowtieOut.close()
    return strOutput + os.sep + "out_bowtie.sam", readNumber


def sortSAMFile(strSAMtools, strSAMFile, strOutput):
    outputFile = strOutput + os.sep + "out_bowtie_sorted.sam"
    strTemp = strSAMtools + os.sep + 'samtools sort -o ' + outputFile + ' -O SAM ' + strSAMFile
    subprocess.call(strTemp, shell=True)
    return outputFile


def removePCRDuplicate(strSAMtools, strSAMFile, strOutput):
    outputFile = strOutput + os.sep + 'out_bowtie_sorted_remove_duplicate.sam'
    strTemp = strSAMtools + os.sep + 'samtools rmdup -sS ' + strSAMFile + ' ' + outputFile
    subprocess.call(strTemp, shell=True)
    return outputFile


def CIGARParser(strSAMFile, strOutput, identity, alnLen):
    CIGAR_Out = open(strOutput + os.sep + 'CIGAR_Out.sam', 'w')
    hitDic = {}
    with open(strSAMFile, "r") as result:
        for line in result:
            if "@" in line:
                CIGAR_Out.write(line)
                if "SQ" in line:
                    clusterName = line.split("\t")[1].replace(config.SAM_SN, "")
                    hitDic[clusterName] = 0
            else:
                info = line.split("\t")
                cigar_str = info[config.sam_cigar_location]
                id, align = calculate_identity(cigar_str, nm_field_location(info))
                if id > identity and align > alnLen:
                    clusterName = info[config.sam_cluster_location]
                    hitDic[clusterName] += 1
                    CIGAR_Out.write(line)
    CIGAR_Out.close()
    return strOutput + os.sep + 'CIGAR_Out.sam', hitDic


def convertSAM2BAM(strSAMtools, strSAMFile, strOutput):
    strTemp = strSAMtools + os.sep + 'samtools view -bS ' + strSAMFile + ' -o ' + strOutput + os.sep + 'out_cigar.bam'
    subprocess.call(strTemp, shell=True)
    return strOutput + os.sep + 'out_cigar.bam'


def runBEDTools(strBEDtools, strBAMFile, strOutput):
    strTemp = strBEDtools + os.sep + 'genomeCoverageBed -ibam ' + strBAMFile + ' -bga > ' + strOutput + os.sep + 'out_bedtools.txt'
    subprocess.call(strTemp, shell=True)
    return strOutput + os.sep + 'out_bedtools.txt'


def abundanceScore(strInput, hitDic, readNumber):
    # Make a dataframe for current sample
    samplesDF = pandas.read_csv(strInput, sep="\t", names=["Cluster", "Start", "End", "Coverage"])
    samplesDF["Sum"] = (samplesDF["End"] - samplesDF["Start"]) * samplesDF["Coverage"]
    samplesDF["Length"] = (samplesDF["End"] - samplesDF["Start"])

    # Make abstract dataframe
    abstractDF = samplesDF.groupby('Cluster')['Sum'].sum().to_frame()
    abstractDF["LengthCluster"] = samplesDF.groupby('Cluster')['Length'].sum()
    tempDF = samplesDF.loc[samplesDF['Sum'] == 0, :]
    abstractDF["LengthZero"] = tempDF.groupby('Cluster')['Length'].sum()
    abstractDF = abstractDF.fillna(0)
    abstractDF["NonZeroPortion"] = 1 - (abstractDF["LengthZero"] / abstractDF["LengthCluster"])
    abstractDF["AverageCoverage"] = (abstractDF["Sum"] / abstractDF["LengthCluster"])

    # Add number of total hit to dataframe
    abstractDF["TotalHit"] = 0
    for item in hitDic:
        abstractDF.set_value(item, "TotalHit", hitDic[item])
    abstractDF["TotalHitNorm"] = (abstractDF["TotalHit"] / readNumber) * 1000000

    abstractDF["AbundanceScore"] = abstractDF["TotalHitNorm"] * abstractDF["NonZeroPortion"] * abstractDF[
        "AverageCoverage"]

    abstractDF.drop('Sum', axis=1, inplace=True)
    abstractDF.drop('LengthZero', axis=1, inplace=True)
    return abstractDF


def analyzeResult(sourceFolder, group, allDataDF):
    flagFirst = True
    resultFiles = glob.glob(sourceFolder + os.sep + "*.csv")
    if len(resultFiles) == 0:
        raise Exception("The folder does not contain any folder! " + sourceFolder)
    for file in resultFiles:
        currentSample = os.path.basename(file).replace(".csv", "")
        sys.stderr.write('Workin on sample: ' + currentSample + '\n')
        currentDF = pandas.read_csv(file)
        if flagFirst:
            allDataDF.loc[:, 'Cluster'] = currentDF.loc[:, 'Cluster']
            allDataDF.set_index('Cluster')
            allDataDF.loc[:, group + "_" + currentSample] = currentDF.loc[:, 'AbundanceScore']
            flagFirst = False
        else:
            allDataDF.loc[:, group + "_" + currentSample] = currentDF.loc[:, 'AbundanceScore']
    return allDataDF


def runRScript(strRscript, input, output, alternative):
    runningDir = os.path.dirname(os.path.abspath(__file__))
    strTemp = strRscript + os.sep + 'Rscript --vanilla ' + runningDir + os.sep + 'wilcoxon.R -i ' + input + ' -o ' + output + ' -a ' + alternative
    subprocess.call(strTemp, shell=True)


def createFolder(strDir):
    currentFolder = str(strDir).split(os.sep)[-1]
    if not os.path.exists(strDir):
        os.makedirs(strDir)
    return strDir


def checkForDependency(strCmd, strArg, strIntendedProgram):
    if strCmd == 'None':
        raise IOError(
            "\nWe can not find " + strIntendedProgram + "in your path.\nPlease check that the program is installed "
                                                        "and the path is correct.\nThe current path for this tool is: "
                                                        "" + strCmd + os.sep + strIntendedProgram + "\n")
        sys.exit(1)
    try:
        if (strArg != ""):
            pCmd = subprocess.Popen([strCmd + os.sep + strIntendedProgram, strArg], stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
        else:
            pCmd = subprocess.Popen([strCmd + os.sep + strIntendedProgram], stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
    except:
        raise IOError(
            "\nWe can not find " + strIntendedProgram + "in your path.\nPlease check that the program is installed "
                                                        "and the path is correct.\nThe current path for this tool is: "
                                                        "" + strCmd + os.sep + strIntendedProgram + "\n")
        sys.exit(1)
    outCmd = pCmd.communicate()[0]
    if (pCmd.returncode == 0):
        sys.stderr.write(
            "Tested " + strIntendedProgram + ". Appears to be working. The current path for this tool is: " + strCmd + os.sep + strIntendedProgram + "\n")
    else:
        sys.stderr.write(
            "Tested " + strIntendedProgram + " returned a nonzero exit code (typically indicates failure). Please "
                                             "check to ensure the program is working.\nThe current path for this tool is: "
            + strCmd + os.sep + strIntendedProgram + "\n")
        sys.exit(1)


def calculate_identity(cigarStr, NM_field):
    NumbersChars = re.compile("\d+")
    NonNumberChars = re.compile("\D+")

    cigar_numbers = NumbersChars.findall(cigarStr)
    cigar_identifiers = NonNumberChars.findall(cigarStr)

    match_mismatch_indel_index = []
    for index, cigar_identifier in enumerate(cigar_identifiers):
        if cigar_identifier in config.sam_match_mismatch_indel:
            match_mismatch_indel_index.append(index)
    try:
        match_mismatch_indel_count = sum([float(cigar_numbers[index]) for index in match_mismatch_indel_index])
    except (IndexError, ValueError):
        match_mismatch_indel_count = 0.0

    NM_field = NM_field.replace(config.SAM_NM, "")
    try:
        matches = match_mismatch_indel_count - int(NM_field)
    except ValueError:
        matches = 0.0

    identity = 0.0
    if match_mismatch_indel_count > 0.0:
        identity = matches / (match_mismatch_indel_count * 1.0)

    return identity, match_mismatch_indel_count


def nm_field_location(info):
    NM_field = ""
    for data in info[config.sam_start_optional_location:]:
        if re.match(config.SAM_NM, data):
            NM_field = data
            break
    return NM_field
